FROM node:latest as build

COPY . /app

WORKDIR /app

RUN npm install
RUN npx ng build --prod

# ============= runtime ================

FROM nginx:latest

USER root

RUN rm -v /etc/nginx/nginx.conf
ADD nginx.conf /etc/nginx/

COPY --from=build /app/dist/frontend /usr/share/nginx/html/

# Expose ports
EXPOSE 80

# Stop container from exiting after command is run
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

CMD service nginx start