import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  flowMinutes: number;
  flowHours: number;
  refillTime: string;
  level = 0;
  warningMessage = '';
  showWarning = false;
  lastUpdate: number;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.getInfo();
    setInterval(this.getInfo, 5000);
  }

  getInfo() {
    this.httpService.getInfo().subscribe((data: {level: number, flow: number, timestamp: number}) => {
      this.level = data.level;

      if (this.level < 0) {
        this.showWarning = true; // show warning if level < 0
        this.warningMessage = "Verificar Sensores"
      }

      this.flowMinutes = data.flow;
      this.flowHours = Math.round((this.flowMinutes * 60) * 100) / 100; // round to 2 decimal places

      this.lastUpdate = data.timestamp;

      const remainingPerc = 100 - this.level;
      const remainingLiters = 5000 * (remainingPerc / 100);

      if (this.level === 100) { // if 100%, time to refill = 0
        return this.refillTime = '-';
      }

      let remainingMinutes, remainingHours;
      if (this.flowMinutes > 0) {
        remainingMinutes = Math.floor(remainingLiters / this.flowMinutes);
        remainingHours = Math.floor(remainingMinutes / 60);
      } else { // if flowMinutes = 0, refillTime would be infinite
        return this.refillTime = '∞';
      }

      if (remainingHours) {
        remainingMinutes = remainingMinutes % 60;
        this.refillTime = `${remainingHours}h${remainingMinutes}min`;
      } else {
        this.refillTime = `${remainingMinutes}min`;
      }
    }, err => {
      console.error(err);
      this.level = -1;
      this.flowHours = -1;
      this.flowMinutes = -1;
    })
  }
}
