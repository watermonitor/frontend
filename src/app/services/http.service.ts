import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  backend = `${window.location.origin}/api/v1`;

  constructor(
    private http: HttpClient
  ) { }

  getInfo() {
    const url = `${this.backend}/info`;
    return this.http.get(url);
  }
}
